terraform {
  version = "~> 0.10.0"
  backend "s3" {}
}

# Variables
variable "access_key" {}
variable "secret_key" {}
variable "region" {
  default = "ap-northeast-1"
}
variable "service_name" {
  default = "example"
}
variable "public_key" {}
variable "amis" {
  default = {
    server = "ami-3bd3c45c" # see. https://aws.amazon.com/jp/amazon-linux-ami/
  }
}
variable "instance_type" {
  default = {
    server = "t2.nano"
  }
}

# AWS Provider
provider "aws" {
  version = "~> 0.1"

  access_key = "${var.access_key}"
  secret_key = "${var.secret_key}"
  region = "${var.region}"
}

## VPC
resource "aws_vpc" "example" {
  cidr_block = "10.0.0.0/16"
  instance_tenancy = "default"
  tags {
    Name = "${var.service_name}-${terraform.workspace}"
  }
}

## Internet Gateway
resource "aws_internet_gateway" "example" {
  vpc_id = "${aws_vpc.example.id}"
  tags {
    Name = "${var.service_name}-${terraform.workspace}"
  }
}

## Subnet
resource "aws_subnet" "public" {
  vpc_id = "${aws_vpc.example.id}"
  cidr_block = "10.0.0.0/24"
  availability_zone = "ap-northeast-1a"
  tags {
    Name = "${var.service_name}-${terraform.workspace}-public-a"
  }
}

## Route Table
resource "aws_default_route_table" "example" {
  default_route_table_id = "${aws_vpc.example.default_route_table_id}"
  tags {
    Name = "${var.service_name}-${terraform.workspace}"
  }
}

resource "aws_route_table" "public" {
  vpc_id = "${aws_vpc.example.id}"
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.example.id}"
  }
  tags {
    Name = "${var.service_name}-${terraform.workspace}-public"
  }
}

resource "aws_route_table_association" "public" {
  subnet_id = "${aws_subnet.public.id}"
  route_table_id = "${aws_route_table.public.id}"
}

## Security Group
resource "aws_default_security_group" "example" {
  vpc_id = "${aws_vpc.example.id}"
  ingress {
    from_port = 0
    to_port = 0
    protocol = -1
    self =true
  }
  egress {
    from_port = 0
    to_port = 0
    protocol = -1
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags {
    Name = "${var.service_name}-${terraform.workspace}"
  }
}

resource "aws_security_group" "ssh" {
  name = "${var.service_name}-${terraform.workspace}-ssh"
  vpc_id = "${aws_vpc.example.id}"
  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags {
    Name = "${var.service_name}-${terraform.workspace}-ssh"
  }
}

resource "aws_security_group" "server" {
  name = "${var.service_name}-${terraform.workspace}-server"
  vpc_id = "${aws_vpc.example.id}"
  ingress {
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port = 443
    to_port = 443
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags {
    Name = "${var.service_name}-${terraform.workspace}-server"
  }
}

## Key Pair
resource "aws_key_pair" "example" {
  key_name = "${var.service_name}-${terraform.workspace}"
  public_key = "${var.public_key}"
}

## EC2
resource "aws_instance" "server" {
  ami = "${var.amis["server"]}"
  instance_type = "${var.instance_type["server"]}"
  key_name = "${aws_key_pair.example.key_name}"
  subnet_id = "${aws_subnet.public.id}"
  vpc_security_group_ids = [
    "${aws_default_security_group.example.id}",
    "${aws_security_group.ssh.id}",
    "${aws_security_group.server.id}",
  ]
  user_data = "${file("user_data.sh")}"
  tags {
    Name = "${var.service_name}-${terraform.workspace}-server"
  }
}

## EIP
resource "aws_eip" "server" {
  vpc = true
}

resource "aws_eip_association" "server" {
  instance_id = "${aws_instance.server.id}"
  allocation_id = "${aws_eip.server.id}"
  allow_reassociation = true
}

## Output
output "SSH" {
  value = "ssh -i /path/to/private_key ec2-user@${aws_eip.server.public_ip}"
}
output "HTTP" {
  value = "curl http://${aws_eip.server.public_ip}"
}
