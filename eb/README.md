# Elastic Beanstalk

* WordPress環境をEBで構築
* Docker HubのWordPressイメージを利用
* AutoScalingで複数台構成
* RDS(Multi-AZ)
* ELBでロードバランス
  - サーバ証明書（自己証明書）を登録してSSLオフロード
* EBのログはCloudWatch Logsに流す

## 構成

![eb](img/eb.png)

## tfstateファイル

このプロジェクトではterraform 0.9以降の[backend](https://www.terraform.io/docs/backends/index.html)
を利用し、`main.tf`でtfstateファイルををS3に保存するように定義しています。

次のような内容の`tfvars`ファイルを作成し`terraform init`を実行してから利用して
ください。

```
bucket="mybucket"
key="path/to/tfstate"
region="ap-northeast-1"
access_key="access key"
secret_key="secret key"
```

```
$ terraform init -backend-config=backend.tfvars
$ terraform plan
...
```

