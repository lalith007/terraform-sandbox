{
  "AWSEBDockerrunVersion": 2,
  "containerDefinitions": [
    {
      "name": "wordpress",
      "image": "wordpress:${docker_image_tag}",
      "essential": true,
      "memory": 256,
      "portMappings": [
        {
          "hostPort": 80,
          "containerPort": 80
        }
      ]
    }
  ]
}
