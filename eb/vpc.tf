# VPC
resource "aws_vpc" "main" {
  cidr_block       = "10.0.0.0/16"
  instance_tenancy = "default"

  tags {
    Name = "${var.service_name}-${terraform.workspace}"
  }
}

# Internet Gateway
resource "aws_internet_gateway" "main" {
  vpc_id = "${aws_vpc.main.id}"
}

# Subnet(public)
resource "aws_subnet" "public" {
  count = "${length(var.az)}"

  vpc_id            = "${aws_vpc.main.id}"
  cidr_block        = "${format("10.0.%d.0/24", count.index + 1)}"
  availability_zone = "${var.region}${var.az[count.index]}"

  tags {
    Name = "${var.service_name}-${terraform.workspace}-public-${var.az[count.index]}"
  }
}

# Route Table(public)
resource "aws_route_table" "public" {
  vpc_id = "${aws_vpc.main.id}"

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.main.id}"
  }

  tags {
    Name = "${var.service_name}-${terraform.workspace}-public"
  }
}

resource "aws_route_table_association" "public" {
  count = "${length(var.az)}"

  subnet_id      = "${element(aws_subnet.public.*.id, count.index)}"
  route_table_id = "${aws_route_table.public.id}"
}
