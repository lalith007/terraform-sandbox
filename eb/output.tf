output "INSTANCES" {
  value = "${aws_elastic_beanstalk_environment.main.instances}"
}

output "CNAME" {
  value = "${aws_elastic_beanstalk_environment.main.cname}"
}
