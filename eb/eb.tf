# Beanstalk Application
resource "aws_elastic_beanstalk_application" "main" {
  name = "${var.service_name}-${terraform.workspace}"
}

# Beanstalk Application Version
resource "aws_elastic_beanstalk_application_version" "main" {
  name = "${var.service_name}-${terraform.workspace}-${var.docker_image_tag}"

  application = "${aws_elastic_beanstalk_application.main.name}"
  bucket      = "${aws_s3_bucket.main.id}"
  key         = "${aws_s3_bucket_object.main.id}"
}

## Create Version
data "archive_file" "main" {
  type        = "zip"
  output_path = "${var.service_name}-${terraform.workspace}-${var.docker_image_tag}.zip"

  source {
    content  = "${data.template_file.dockerrun.rendered}"
    filename = "Dockerrun.aws.json"
  }
}

data "template_file" "dockerrun" {
  template = "${file("Dockerrun.aws.json.tpl")}"

  vars {
    docker_image_tag = "${var.docker_image_tag}"
  }
}

## Elastic Beanstalk Application Version File on S3
resource "aws_s3_bucket_object" "main" {
  bucket = "${aws_s3_bucket.main.id}"
  key    = "beanstalk/application_version/${var.service_name}-${terraform.workspace}-${var.docker_image_tag}.zip"
  source = "${data.archive_file.main.output_path}"
}

# Beanstalk Environment
resource "aws_elastic_beanstalk_environment" "main" {
  name = "${var.service_name}-${terraform.workspace}"

  application         = "${aws_elastic_beanstalk_application.main.name}"
  cname_prefix        = "${aws_elastic_beanstalk_application.main.name}"
  tier                = "WebServer"
  solution_stack_name = "64bit Amazon Linux 2017.03 v2.7.4 running Multi-container Docker 17.03.1-ce (Generic)"
  version_label       = "${aws_elastic_beanstalk_application_version.main.name}"

  # https://docs.aws.amazon.com/elasticbeanstalk/latest/dg/command-options-general.html

  # Software
  setting {
    namespace = "aws:elasticbeanstalk:cloudwatch:logs"
    name      = "StreamLogs"
    value     = "true"
  }
  setting {
    namespace = "aws:elasticbeanstalk:cloudwatch:logs"
    name      = "DeleteOnTerminate"
    value     = "true"
  }
  # NOTE: 環境変数でDB(RDS)の情報を渡す
  setting {
    namespace = "aws:elasticbeanstalk:application:environment"
    name      = "WORDPRESS_DB_HOST"
    value     = "${aws_db_instance.main.address}:${aws_db_instance.main.port}"
  }
  setting {
    namespace = "aws:elasticbeanstalk:application:environment"
    name      = "WORDPRESS_DB_USER"
    value     = "${aws_db_instance.main.username}"
  }
  setting {
    namespace = "aws:elasticbeanstalk:application:environment"
    name      = "WORDPRESS_DB_PASSWORD"
    value     = "${aws_db_instance.main.password}"
  }

  # Instance
  setting {
    namespace = "aws:autoscaling:launchconfiguration"
    name      = "InstanceType"
    value     = "${var.instance_type["instance"]}"
  }

  # Capacity
  setting {
    namespace = "aws:autoscaling:asg"
    name      = "MinSize"
    value     = 2
  }
  setting {
    namespace = "aws:autoscaling:asg"
    name      = "MaxSize"
    value     = 4
  }

  # LoadBalancer
  setting {
    namespace = "aws:elb:loadbalancer"
    name      = "CrossZone"
    value     = "true"
  }
  setting {
    namespace = "aws:elb:listener:443"
    name      = "ListenerProtocol"
    value     = "HTTPS"
  }
  setting {
    namespace = "aws:elb:listener:443"
    name      = "InstancePort"
    value     = 80
  }
  setting {
    namespace = "aws:elb:listener:443"
    name      = "SSLCertificateId"
    value     = "${aws_iam_server_certificate.main.arn}"
  }
  setting {
    namespace = "aws:elb:policies"
    name      = "LoadBalancerPorts"
    value     = ":all"
  }
  setting {
    namespace = "aws:elb:policies"
    name      = "Stickiness Cookie Expiration"
    value     = "604800" # 7 days
  }
  setting {
    namespace = "aws:elb:policies"
    name      = "Stickiness Policy"
    value     = "true"
  }

  # Rolling update and deploy
  setting {
    namespace = "aws:autoscaling:updatepolicy:rollingupdate"
    name      = "RollingUpdateEnabled"
    value     = "true"
  }

  # Security
  setting {
    namespace = "aws:autoscaling:launchconfiguration"
    name      = "EC2KeyName"
    value     = "${aws_key_pair.main.key_name}"
  }
  setting {
    namespace = "aws:autoscaling:launchconfiguration"
    name      = "IamInstanceProfile"
    value     = "aws-elasticbeanstalk-ec2-role"
  }

  # Monitoring
  setting {
    namespace = "aws:elasticbeanstalk:application"
    name      = "Application Healthcheck URL"
    # value     = "/"
    value     = "/readme.html" # TODO: WordPressのインストール完了まで "/" は302を返すのでヘルスチェックがエラーとなる。インストール前は "/readme.html" をチェック対象としてこれを回避する。
  }

  # Notification

  # Networking
  setting {
    namespace = "aws:ec2:vpc"
    name      = "VPCId"
    value     = "${aws_vpc.main.id}"
  }
  setting {
    namespace = "aws:ec2:vpc"
    name      = "Subnets"
    value     = "${join(",", aws_subnet.public.*.id)}"
  }
  setting {
    namespace = "aws:ec2:vpc"
    name      = "AssociatePublicIpAddress"
    value     = "true"
  }
  setting {
    namespace = "aws:autoscaling:launchconfiguration"
    name      = "SecurityGroups"
    value     = "${join(",", list(aws_default_security_group.main.id, aws_security_group.ssh.id, aws_security_group.myapp.id))}"
  }
}
