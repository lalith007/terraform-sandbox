resource "aws_db_instance" "main" {
  allocated_storage      = 5
  storage_type           = "gp2"
  engine                 = "mysql"
  engine_version         = "${var.mysql_engine_version}"
  instance_class         = "${var.instance_type["db"]}"
  name                   = "wordpress"
  username               = "${var.db_username}"
  password               = "${var.db_password}"
  multi_az               = "true"
  db_subnet_group_name   = "${aws_db_subnet_group.main.id}"
  parameter_group_name   = "${aws_db_parameter_group.main.id}"
  vpc_security_group_ids = ["${aws_default_security_group.main.id}"]
  skip_final_snapshot    = "true"
}

resource "aws_db_subnet_group" "main" {
  name = "${var.service_name}-${terraform.workspace}"

  subnet_ids = ["${aws_subnet.public.*.id}"]

  tags {
    Name = "${var.service_name}-${terraform.workspace}"
  }
}

resource "aws_db_parameter_group" "main" {
  name = "${var.service_name}-${terraform.workspace}"

  family = "mysql5.6"

  parameter {
    name  = "character_set_server"
    value = "utf8"
  }

  parameter {
    name  = "character_set_client"
    value = "utf8"
  }
}
