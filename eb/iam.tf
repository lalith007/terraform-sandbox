# IAM Server Certificate
# http://docs.aws.amazon.com/ja_jp/IAM/latest/UserGuide/id_credentials_server-certs.html
resource "aws_iam_server_certificate" "main" {
  name = "${var.service_name}-${terraform.workspace}-self_signed_cert"

  certificate_body = "${tls_self_signed_cert.main.cert_pem}"
  private_key      = "${tls_private_key.main.private_key_pem}"
}

resource "tls_private_key" "main" {
  algorithm   = "ECDSA" # or "RSA"
  ecdsa_curve = "P256"
}

resource "tls_self_signed_cert" "main" {
  key_algorithm         = "${tls_private_key.main.algorithm}"
  private_key_pem       = "${tls_private_key.main.private_key_pem}"
  validity_period_hours = 12

  subject {
    country      = "JP"
    common_name  = "localhost"
    organization = "Test Corp. Inc"
  }

  allowed_uses = [
    "key_encipherment",
    "digital_signature",
    "server_auth",
  ]
}
