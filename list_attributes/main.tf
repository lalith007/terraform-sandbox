data "http" "test" {
  count = 3
  url = "http://httpbin.org/get?count=${count.index}"
}

output "list" {
  value = "${data.http.test.*.body}"
}

# no output
output "single value" {
  value = "${data.http.test.body}"
}
