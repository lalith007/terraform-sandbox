data "archive_file" "kinesis2bq" {
  type        = "zip"
  source_dir  = "lambda/kinesis2bq"
  output_path = "lambda/kinesis2bq.zip"
}

resource "aws_lambda_function" "kinesis2bq" {
  filename         = "lambda/kinesis2bq.zip"
  function_name    = "${var.service_name}-${terraform.workspace}-kinesis2bq"
  role             = "${aws_iam_role.kinesis_lambda.arn}"
  handler          = "kinesis2bq.handler"
  source_code_hash = "${data.archive_file.kinesis2bq.output_base64sha256}"
  runtime          = "python2.7"

  # キーを含めて4KBまで
  environment {
    variables = {
      BQ_CREDENTIALS = "${data.aws_kms_ciphertext.bq_credentials.ciphertext_blob}"
      BQ_PROJECT     = "${var.gcp["project"]}"
      BQ_DATASET     = "${var.bq["dataset"]}"
      BQ_TABLE       = "${var.bq["table"]}"
    }
  }

  tags {
    Name        = "${var.service_name}"
    Environment = "${terraform.workspace}"
  }
}

resource "aws_lambda_event_source_mapping" "kinesis2bq" {
  batch_size        = 100 # default 100
  event_source_arn  = "${aws_kinesis_stream.main.arn}"
  function_name     = "${aws_lambda_function.kinesis2bq.arn}"
  starting_position = "TRIM_HORIZON"
}
