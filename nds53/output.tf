output "Kinesis stream name" {
  value = "${aws_kinesis_stream.main.name}"
}
