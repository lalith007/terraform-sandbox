# AWS Credentials
variable "aws" {
  default = {
    access_key = ""
    secret_key = ""
    region     = "ap-northeast-1"
  }
}

# GCP Credentials
variable "gcp" {
  default = {
    credentials = "" # file path
    project     = ""
    region      = ""
  }
}

# Service Name
variable "service_name" {
  default = "nds53"
}

# BigQuery
variable "bq" {
  default = {
    credentials = "" # file path
    dataset     = "nds53"
    table       = "myapp"
  }
}
