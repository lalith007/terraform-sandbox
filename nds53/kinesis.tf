resource "aws_kinesis_stream" "main" {
  name = "${var.service_name}-${terraform.workspace}"

  shard_count = 1

  shard_level_metrics = [
    "IncomingBytes",
    "OutgoingBytes",
  ]

  tags {
    Name        = "${var.service_name}"
    Environment = "${terraform.workspace}"
  }
}
