resource "google_bigquery_dataset" "main" {
  dataset_id = "${var.bq["dataset"]}"

  labels {
    name = "${var.service_name}"
    env  = "${terraform.workspace}"
  }
}

resource "google_bigquery_table" "main" {
  dataset_id = "${google_bigquery_dataset.main.dataset_id}"
  table_id   = "${var.bq["table"]}"
  schema     = "${file("bq/schema.json")}"

  labels {
    name = "${var.service_name}"
    env  = "${terraform.workspace}"
  }
}
