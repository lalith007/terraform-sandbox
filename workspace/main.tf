terraform {
  version = "~>0.10.0"
}

output "terraform.workspace" {
  value = "${terraform.workspace}"
}

output "test" {
  value = "${terraform.workspace == "default" ? "hello" : "goodbye"}"
}
