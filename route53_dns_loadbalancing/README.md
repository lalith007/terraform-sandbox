# Route53によるDNSロードバランシング

* お名前.com や freenom.com などのレジストラでドメインを取得
  - DNSサーバをRoute53に設定する
* Route53でゾーン設定
  - weightedポリシー
    - すべてのレコードのweightを1にして均等に結果を返す
  - ヘルスチェック

## 構成

![route53_dns_loadbalancing](img/route53_dns_loadbalancing.png)

## tfstateファイル

このプロジェクトではterraform 0.9以降の[backend](https://www.terraform.io/docs/backends/index.html)
を利用し、`main.tf`でtfstateファイルををS3に保存するように定義しています。

次のような内容の`tfvars`ファイルを作成し`terraform init`を実行してから利用して
ください。

```
bucket="mybucket"
key="path/to/tfstate"
region="ap-northeast-1"
access_key="access key"
secret_key="secret key"
```

```
$ terraform init -backend-config=backend.tfvars
$ terraform plan
...
```

