terraform {
  version = "~> 0.10.0"
  backend "s3" {}
}

provider "aws" {
  version = "~> 0.1"

  access_key = "${var.access_key}"
  secret_key = "${var.secret_key}"
  region = "${var.region}"
}
