resource "aws_lightsail_key_pair" "main" {
  name = "${var.service_name}-${terraform.workspace}"

  public_key = "${var.public_key}"
}

resource "aws_lightsail_static_ip_attachment" "main" {
  count = "${var.instance_count}"

  static_ip_name = "${element(aws_lightsail_static_ip.main.*.name, count.index)}"
  instance_name  = "${element(aws_lightsail_instance.main.*.name, count.index)}"
}

resource "aws_lightsail_static_ip" "main" {
  count = "${var.instance_count}"

  name = "${var.service_name}-${terraform.workspace}-${count.index}"
}

resource "aws_lightsail_instance" "main" {
  count = "${var.instance_count}"

  name = "${var.service_name}-${terraform.workspace}-${format("%02d", (count.index / length(var.az)) + 1)}${element(var.az, count.index % length(var.az))}"

  availability_zone = "${var.region}${element(var.az, count.index % length(var.az))}"
  blueprint_id      = "${var.lightsail_blueprint}"
  bundle_id         = "${var.lightsail_bundle}"
  key_pair_name     = "${aws_lightsail_key_pair.main.id}"
}
