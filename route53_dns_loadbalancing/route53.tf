resource "aws_route53_zone" "main" {
  name = "${var.domain}"

  tags {
    Name = "${var.service_name}-${terraform.workspace}"
  }
}

resource "aws_route53_health_check" "main" {
  count = "${var.instance_count}"

  ip_address = "${element(aws_lightsail_static_ip.main.*.ip_address, count.index)}"
  fqdn = "${var.domain}"
  port = 80
  type = "HTTP"
  resource_path = "/"
  request_interval = 30
  failure_threshold = 3

  tags {
    Name = "${var.service_name}-${terraform.workspace}-${element(aws_lightsail_static_ip.main.*.ip_address, count.index)}"
  }
}

resource "aws_route53_record" "main" {
  count = "${var.instance_count}"

  name = "${var.domain}"

  zone_id         = "${aws_route53_zone.main.id}"
  type            = "A"
  ttl             = 60
  records         = ["${element(aws_lightsail_static_ip.main.*.ip_address, count.index)}"]
  health_check_id = "${element(aws_route53_health_check.main.*.id, count.index)}"
  set_identifier  = "${var.service_name}-${terraform.workspace}-${element(aws_lightsail_static_ip.main.*.ip_address, count.index)}"

  weighted_routing_policy {
    weight = 1
  }
}
