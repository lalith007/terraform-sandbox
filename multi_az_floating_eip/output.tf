output "BASTION_IP" {
  value = "${aws_eip.bastion.public_ip}"
}

output "SERVER_IP" {
  value = "${aws_eip.server.public_ip}"
}

output "HOST_IP" {
  value = "${aws_instance.server.*.private_ip}"
}
