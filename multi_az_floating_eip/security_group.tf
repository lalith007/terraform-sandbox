# Default Security Group
resource "aws_default_security_group" "default" {
  vpc_id = "${aws_vpc.default.id}"
  ingress {
    from_port = 0
    to_port = 0
    protocol = -1
    self = true
  }
  egress {
    from_port = 0
    to_port = 0
    protocol = -1
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags {
    Name = "${var.service_name}-${terraform.workspace}"
  }
}

# Security Group(bastion)
resource "aws_security_group" "bastion" {
  name = "${var.service_name}-${terraform.workspace}-bastion"
  description = "${var.service_name}-${terraform.workspace}-bastion"
  vpc_id = "${aws_vpc.default.id}"
  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags {
    Name = "${var.service_name}-${terraform.workspace}-bastion"
  }
}

# Security Group(ssh)
resource "aws_security_group" "ssh" {
  name = "${var.service_name}-${terraform.workspace}-ssh"
  description = "${var.service_name}-${terraform.workspace}-ssh"
  vpc_id = "${aws_vpc.default.id}"
  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    security_groups = ["${aws_default_security_group.default.id}"]
  }
  tags {
    Name = "${var.service_name}-${terraform.workspace}-ssh"
  }
}

# Security Group(web)
resource "aws_security_group" "web" {
  name = "${var.service_name}-${terraform.workspace}-web"
  description = "${var.service_name}-${terraform.workspace}-web"
  vpc_id = "${aws_vpc.default.id}"
  ingress {
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags {
    Name = "${var.service_name}-${terraform.workspace}-web"
  }
}
