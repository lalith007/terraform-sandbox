# Floating ElasticIP

* マルチAZのサブネットを作成
* ホスト間をVRRPで冗長化(Active-Standby)
* フェイルオーバー時にElasticIPを付け替える

## 構成

![multi_az_floating_eip](img/multi_az_floating_eip.png)


## 確認

`keepalived`を停止/起動してElasticIP(SERVER_IP)が付け変わることを確認し、ウェブ
クライアントでElasticIP(SERVER_IP)にアクセスできることを確認してください。

```
$ curl $SERVER_IP
```


## tfstateファイル

このプロジェクトではterraform 0.9以降の[backend](https://www.terraform.io/docs/backends/index.html)
を利用し、`main.tf`でtfstateファイルををS3に保存するように定義しています。

次のような内容の`tfvars`ファイルを作成し`terraform init`を実行してから利用して
ください。

```
bucket="mybucket"
key="path/to/tfstate"
region="ap-northeast-1"
access_key="access key"
secret_key="secret key"
```

```
$ terraform init -backend-config=terraform.backend.tfvars
$ terraform plan
...
```

