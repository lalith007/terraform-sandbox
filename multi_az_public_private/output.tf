output "BASTION_IP" {
  value = "${aws_instance.bastion.*.public_ip}"
}
