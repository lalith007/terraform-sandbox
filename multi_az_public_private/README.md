# マルチAZなパブリック/プライベートサブネットの構築

* マルチAZ
* パブリックとプライベートのサブネットを作成
* プライベートサブネットからのアウトバウンドはManaged NAT Gatewayを利用
  * Managed NAT GatewayをAZごとに用意
* 踏み台サーバはEIPをアタッチし
  * セキュリティグループでインターネットからのアクセスを許可する
  * 可能であれば 0.0.0.0/0 ではなく/32や特定のネットワークに絞る
* ホストはセキュリティグループで踏み台サーバからのSSHアクセスを許可する


## 構成

![multi_az_public_private](img/multi_az_public_private.png)


## tfstateファイル

このプロジェクトではterraform 0.9以降の[backend](https://www.terraform.io/docs/backends/index.html)
を利用し、`main.tf`でtfstateファイルををS3に保存するように定義しています。

次のような内容の`tfvars`ファイルを作成し`terraform init`を実行してから利用して
ください。

```
bucket="mybucket"
key="path/to/tfstate"
region="ap-northeast-1"
access_key="access key"
secret_key="secret key"
```

```
$ terraform init -backend-config=terraform.backend.tfvars
$ terraform plan
...
```

