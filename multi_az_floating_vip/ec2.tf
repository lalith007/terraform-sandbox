# Key Pair
resource "aws_key_pair" "default" {
  key_name = "${var.service_name}-${terraform.workspace}"
  public_key = "${var.public_key}"
}

# EC2(bastion + client)
resource "aws_instance" "bastion" {
  ami = "${var.amis["ap-northeast-1"]}"
  instance_type = "${var.instance_type["bastion"]}"
  key_name = "${aws_key_pair.default.key_name}"
  vpc_security_group_ids = [
    "${aws_default_security_group.default.id}",
    "${aws_security_group.bastion.id}",
  ]
  subnet_id = "${element(aws_subnet.public.*.id, 0)}"
  associate_public_ip_address = true
  private_ip = "${cidrhost(element(aws_subnet.public.*.cidr_block, 0), 4)}"
  tags {
    Name = "${var.service_name}-${terraform.workspace}-bastion"
    Role = "bastion"
  }
}

# EC2(server)
resource "aws_instance" "server" {
  count = "${length(var.az)}"
  ami = "${var.amis["ap-northeast-1"]}"
  instance_type = "${var.instance_type["server"]}"
  key_name = "${aws_key_pair.default.key_name}"
  vpc_security_group_ids = [
    "${aws_default_security_group.default.id}",
    "${aws_security_group.ssh.id}",
    "${aws_security_group.web.id}",
  ]
  subnet_id = "${element(aws_subnet.public.*.id, count.index % length(var.az))}"
  associate_public_ip_address = true
  private_ip = "${cidrhost(element(aws_subnet.public.*.cidr_block, count.index % length(var.az)), 10 + (count.index / length(var.az)))}"
  source_dest_check = false
  iam_instance_profile = "${aws_iam_instance_profile.server.name}"
  user_data="${file("provision/server.sh")}"
  tags {
    Name = "${var.service_name}-${terraform.workspace}-server${format("%02d", (count.index / length(var.az)) + 1)}${var.az[count.index % length(var.az)]}"
    Role = "server"
  }
}

# EIP(basiton + client)
resource "aws_eip" "bastion" {
  vpc = true
}

resource "aws_eip_association" "bastion" {
  instance_id = "${aws_instance.bastion.id}"
  allocation_id = "${aws_eip.bastion.id}"
}

# Provisioning(server)
resource "null_resource" "server" {
  count = "${length(var.az)}"

  triggers {
    eip_bastion_id = "${aws_eip.bastion.id}"
    server_ids = "${join(",", aws_instance.server.*.id)}"
  }

  provisioner "file" {
    source = "provision/server.sh"
    destination = "/tmp/provision.sh"
  }

  provisioner "remote-exec" {
    inline = [
      "chmod +x /tmp/provision.sh",
      "sudo /tmp/provision.sh",
    ]
  }

  connection {
    host = "${element(aws_instance.server.*.private_ip, count.index % length(var.az))}"
    private_key = "${file(var.private_key_path)}"
    user = "ec2-user"
    bastion_host = "${aws_eip.bastion.public_ip}"
    bastion_user = "ec2-user"
    bastion_private_key = "${file(var.private_key_path)}"
  }
}
