# マルチAZな仮想IPアドレスによるFloating IP

* マルチAZのサブネットを作成
* ホスト間をVRRPで冗長化(Active-Standby)
* フェイルオーバー時はルーティングテーブルを書き換える
  * VIPはVPCのCIDR範囲外のものを使う(192.168.1.1)
  * VIP(192.168.1.1/32)のルーティング先インスタンス(ENI)を変更する

## 構成

![multi_az_floating_vip](img/multi_az_floating_vip.png)


## 環境構築

`keepalived`を停止/起動してVIP(192.168.1.1)が付け変わることを`ip addr`で確認し、
client(bastion)ホストから`curl 192.168.1.1`で接続できることを確認してください。

```
bastion$ curl 192.168.1.1
```


## tfstateファイル

このプロジェクトではterraform 0.9以降の[backend](https://www.terraform.io/docs/backends/index.html)
を利用し、`main.tf`でtfstateファイルををS3に保存するように定義しています。

次のような内容の`tfvars`ファイルを作成し`terraform init`を実行してから利用して
ください。

```
bucket="mybucket"
key="path/to/tfstate"
region="ap-northeast-1"
access_key="access key"
secret_key="secret key"
```

```
$ terraform init -backend-config=terraform.backend.tfvars
$ terraform plan
...
```

