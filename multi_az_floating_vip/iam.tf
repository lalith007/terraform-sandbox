resource "aws_iam_instance_profile" "server" {
  name  = "${var.service_name}-${terraform.workspace}-server"
  role = "${aws_iam_role.server.name}"
}

resource "aws_iam_role" "server" {
  name = "${var.service_name}-${terraform.workspace}-server"
  assume_role_policy = "${data.aws_iam_policy_document.server_assume_role_policy.json}"
}

resource "aws_iam_role_policy" "server" {
  name = "${var.service_name}-${terraform.workspace}-server_policy"
  role = "${aws_iam_role.server.id}"
  policy = "${data.aws_iam_policy_document.server_policy.json}"
}

data "aws_iam_policy_document" "server_assume_role_policy" {
  statement {
    actions = ["sts:AssumeRole"]
    principals {
      type = "Service"
      identifiers = ["ec2.amazonaws.com"]
    }
  }
}

data "aws_iam_policy_document" "server_policy" {
  statement {
    actions = [
      "ec2:DeleteRoute",
      "ec2:CreateRoute",
      "ec2:DescribeInstances",
      "ec2:DescribeRouteTables",
    ]
    resources = [
      "*",
    ]
  }
}
