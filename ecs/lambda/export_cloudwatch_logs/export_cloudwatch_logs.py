import datetime
import time
import os
import boto3

log_group_name     = os.environ['LOG_GROUP_NAME']
destination        = os.environ['DESTINATION']
destination_prefix = 'cloudwatch_logs/{0}'.format(datetime.date.today() - datetime.timedelta(days = 1))

def get_from_timestamp():
    today     = datetime.date.today()
    yesterday = datetime.datetime.combine(today - datetime.timedelta(days = 1), datetime.time(0, 0, 0))
    timestamp = time.mktime(yesterday.timetuple())
    return int(timestamp)

def get_to_timestamp(from_ts):
    return from_ts + (60 * 60 * 24 ) - 1

def lambda_handler(event, context):
    from_ts = get_from_timestamp()
    to_ts = get_to_timestamp(from_ts)

    print 'Timestamp: from {0} to {1}'.format(from_ts, to_ts)

    client = boto3.client('logs')

    res = client.create_export_task(
        logGroupName      = log_group_name,
        fromTime          = from_ts * 1000,
        to                = to_ts * 1000,
        destination       = destination,
        destinationPrefix = destination_prefix,
    )

    return res
