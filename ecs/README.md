# ECS

* Frontend(nginx) + Backend(mojolicious) をECSで構築
* コンテナレジストリはECRを利用
* ECS Container Instance はAutoScalingで構築
* ELBでロードバランス
  - サーバ証明書（自己証明書）を登録してSSLオフロード
  - BackendのサービスディスカバリのためにInternal ELBも構築
  - ログをS3に保存
* コンテナのログはCloudWatch Logsに流す
  - Lambdaで定期的にS3にエクスポート

## 構成

![ecs_ecs](img/ecs_ecs.png)

## ロードバランサ

![ecs_elb](img/ecs_elb.png)

## ログ

![ecs_logs](img/ecs_logs.png)


## tfstateファイル

このプロジェクトではterraform 0.9以降の[backend](https://www.terraform.io/docs/backends/index.html)
を利用し、`main.tf`でtfstateファイルををS3に保存するように定義しています。

次のような内容の`tfvars`ファイルを作成し`terraform init`を実行してから利用して
ください。

```
bucket="mybucket"
key="path/to/tfstate"
region="ap-northeast-1"
access_key="access key"
secret_key="secret key"
```

```
$ terraform init -backend-config=backend.tfvars
$ terraform plan
...
```

