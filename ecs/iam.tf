# Role(ECS Instance)
resource "aws_iam_role" "ecs_instance_role" {
  name = "${var.service_name}-${terraform.workspace}-ecs_instance_role"

  assume_role_policy = "${data.aws_iam_policy_document.ecs_instance_role.json}"
}

data "aws_iam_policy_document" "ecs_instance_role" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ec2.amazonaws.com"]
    }
  }
}

resource "aws_iam_policy_attachment" "ecs_instance_role" {
  name = "${var.service_name}-${terraform.workspace}-ecs_instance_role"

  roles      = ["${aws_iam_role.ecs_instance_role.name}"]
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonEC2ContainerServiceforEC2Role"
}

resource "aws_iam_instance_profile" "ecs" {
  name = "${var.service_name}-${terraform.workspace}"
  role = "${aws_iam_role.ecs_instance_role.name}"
}

# Role(ECS Service)
resource "aws_iam_role" "ecs_service_role" {
  name = "${var.service_name}-${terraform.workspace}-ecs_service_role"

  assume_role_policy = "${data.aws_iam_policy_document.ecs_service_role.json}"
}

data "aws_iam_policy_document" "ecs_service_role" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ecs.amazonaws.com"]
    }
  }
}

resource "aws_iam_policy_attachment" "ecs_service_role" {
  name = "${var.service_name}-${terraform.workspace}-ecs_service_role"

  roles      = ["${aws_iam_role.ecs_service_role.name}"]
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonEC2ContainerServiceRole"
}

# IAM Server Certificate
# http://docs.aws.amazon.com/ja_jp/IAM/latest/UserGuide/id_credentials_server-certs.html
resource "aws_iam_server_certificate" "main" {
  name = "${var.service_name}-${terraform.workspace}-self_signed_cert"

  certificate_body = "${tls_self_signed_cert.main.cert_pem}"
  private_key      = "${tls_private_key.main.private_key_pem}"
}

resource "tls_private_key" "main" {
  algorithm   = "ECDSA" # or "RSA"
  ecdsa_curve = "P256"
}

resource "tls_self_signed_cert" "main" {
  key_algorithm         = "${tls_private_key.main.algorithm}"
  private_key_pem       = "${tls_private_key.main.private_key_pem}"
  validity_period_hours = 12

  subject {
    country      = "JP"
    common_name  = "localhost"
    organization = "Test Corp. Inc"
  }

  allowed_uses = [
    "key_encipherment",
    "digital_signature",
    "server_auth",
  ]
}

# Lambda(Export CloudWatch Logs)
resource "aws_iam_role" "export_cloudwatch_logs" {
  name = "${var.service_name}-${terraform.workspace}-export_cloudwatch_logs"

  assume_role_policy= "${data.aws_iam_policy_document.export_cloudwatch_logs_assume_role_policy.json}" 
}

resource "aws_iam_role_policy" "export_cloudwatch_logs" {
  name = "${var.service_name}-export_cloudwatch_logs"

  role = "${aws_iam_role.export_cloudwatch_logs.id}"
  policy = "${data.aws_iam_policy_document.export_cloudwatch_logs_role_policy.json}" 
}

data "aws_iam_policy_document" "export_cloudwatch_logs_assume_role_policy" {
  statement {
    actions = [
      "sts:AssumeRole",
    ]
    principals {
      type        = "Service"
      identifiers = ["lambda.amazonaws.com"]
    }
  }
}

data "aws_iam_policy_document" "export_cloudwatch_logs_role_policy" {
  statement {
    actions   = ["logs:CreateExportTask"]
    resources = ["${aws_cloudwatch_log_group.main.arn}"]
  }
  statement {
    actions = [
      "logs:CreateLogGroup",
      "logs:CreateLogStream",
      "logs:PutLogEvents",
    ]
    resources = [
      "arn:aws:logs:*:*:*",
    ]
  }
}
