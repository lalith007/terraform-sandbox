#!/bin/sh

NGINX_SERVER_CONFIG=/etc/nginx/conf.d/default.conf

cat <<EOF >$NGINX_SERVER_CONFIG
upstream api {
    server $(echo $BACKEND_PORT | sed 's/tcp:\/\///');
}

server {
    listen 80 default_server;
    listen [::]:80 default_server;

    location / {
        proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
        proxy_pass http://api/;
    }
}
EOF

ln -sf /dev/stdout /var/log/nginx/access.log
ln -sf /dev/stderr /var/log/nginx/error.log

nginx -g 'daemon off;'
