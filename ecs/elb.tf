# ELB(frontend)
resource "aws_elb" "frontend" {
  name = "${var.service_name}-${terraform.workspace}-frontend"

  subnets                     = ["${aws_subnet.public.*.id}"]
  cross_zone_load_balancing   = true
  idle_timeout                = 400
  connection_draining         = true
  connection_draining_timeout = 400

  security_groups =[
    "${aws_default_security_group.main.id}",
    "${aws_security_group.frontend.id}",
  ]
  
  listener {
    lb_port           = 80
    lb_protocol       = "http"
    instance_port     = 80
    instance_protocol = "http"
  }

  listener {
    lb_port            = 443
    lb_protocol        = "https"
    instance_port      = 80
    instance_protocol  = "http"
    ssl_certificate_id = "${aws_iam_server_certificate.main.arn}"
  }

  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 5
    target              = "HTTP:80/"
    interval            = 30
  }

  access_logs {
    bucket        = "${aws_s3_bucket.main.id}"
    bucket_prefix = "frontend"
    interval      = 5
  }

  tags {
    Name = "${var.service_name}-${terraform.workspace}-frontend"
  }
}

# ELB(backend)
resource "aws_elb" "backend" {
  name = "${var.service_name}-${terraform.workspace}-backend"

  subnets                     = ["${aws_subnet.public.*.id}"]
  cross_zone_load_balancing   = true
  idle_timeout                = 400
  connection_draining         = true
  connection_draining_timeout = 400
  internal                    = true

  security_groups =[
    "${aws_default_security_group.main.id}",
  ]

  listener {
    instance_port     = 3000
    instance_protocol = "http"
    lb_port           = 3000
    lb_protocol       = "http"
  }

  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 5
    target              = "HTTP:3000/"
    interval            = 30
  }

  access_logs {
    bucket        = "${aws_s3_bucket.main.id}"
    bucket_prefix = "backend"
    interval      = 5
  }

  tags {
    Name = "${var.service_name}-${terraform.workspace}-backend"
  }
}
