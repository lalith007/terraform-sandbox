# CloudWatch Logs
resource "aws_cloudwatch_log_group" "main" {
  name = "${var.service_name}-${terraform.workspace}"

  tags {
    Name = "${var.service_name}-${terraform.workspace}"
  }
}
