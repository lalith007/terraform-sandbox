resource "aws_ecr_repository" "main" {
  count = "${length(var.containers)}"
  name  = "${var.service_name}-${terraform.workspace}-${var.containers[count.index]}"
}

resource "aws_ecs_cluster" "main" {
  name = "${var.service_name}-${terraform.workspace}-cluster"
}

data "template_file" "task_definition-frontend" {
  template = "${file("task-definitions/frontend.json")}"

  vars {
    backend_port          = "tcp://${aws_elb.backend.dns_name}:3000"
    awslogs_group         = "${aws_cloudwatch_log_group.main.name}"
    awslogs_stream_prefix = "${var.service_name}-${terraform.workspace}"
  }
}

resource "aws_ecs_task_definition" "frontend" {
  family                = "${var.service_name}-${terraform.workspace}-frontend"
  container_definitions = "${data.template_file.task_definition-frontend.rendered}"
}

resource "aws_ecs_service" "frontend" {
    name = "${var.service_name}-${terraform.workspace}-frontend"

    cluster         = "${aws_ecs_cluster.main.id}"
    task_definition = "${aws_ecs_task_definition.frontend.arn}"
    iam_role        = "${aws_iam_role.ecs_service_role.arn}"

    desired_count                      = 2
    deployment_minimum_healthy_percent = 50
    deployment_maximum_percent         = 100

    load_balancer {
        elb_name       = "${aws_elb.frontend.name}"
        container_name = "frontend"
        container_port = 80
    }
}

data "template_file" "task_definition-backend" {
  template = "${file("task-definitions/backend.json")}"

  vars {
    awslogs_group         = "${aws_cloudwatch_log_group.main.name}"
    awslogs_stream_prefix = "${var.service_name}-${terraform.workspace}"
  }
}

resource "aws_ecs_task_definition" "backend" {
  family                = "${var.service_name}-${terraform.workspace}-backend"
  container_definitions = "${data.template_file.task_definition-backend.rendered}"
}

resource "aws_ecs_service" "backend" {
    name = "${var.service_name}-${terraform.workspace}-backend"

    cluster         = "${aws_ecs_cluster.main.id}"
    task_definition = "${aws_ecs_task_definition.backend.arn}"
    iam_role        = "${aws_iam_role.ecs_service_role.arn}"

    desired_count                      = 2
    deployment_minimum_healthy_percent = 50
    deployment_maximum_percent         = 100

    load_balancer {
        elb_name       = "${aws_elb.backend.name}"
        container_name = "backend"
        container_port = 3000
    }
}

