resource "aws_s3_bucket" "main" {
  bucket        = "${var.service_name}-${terraform.workspace}"
  acl           = "private"
  policy        = "${data.aws_iam_policy_document.main.json}"
  force_destroy = true

  tags {
    Name = "${var.service_name}-${terraform.workspace}"
  }
}

data "aws_iam_policy_document" "main" {
  # for ELB
  statement {
    actions   = ["s3:PutObject"]
    resources = ["arn:aws:s3:::${var.service_name}-${terraform.workspace}/*"]

    principals {
    type        = "AWS"
    # identifiers = ["arn:aws:iam::582318560864:root"]
    identifiers = ["582318560864"]
    }
  }

  # for CloudWatch Logs
  statement {
    actions   = ["s3:GetBucketAcl"]
    resources = ["arn:aws:s3:::${var.service_name}-${terraform.workspace}"]

    principals {
      type        = "Service"
      identifiers = ["logs.ap-northeast-1.amazonaws.com"]
    }
  }

  statement {
    actions   = ["s3:PutObject"]
    resources = ["arn:aws:s3:::${var.service_name}-${terraform.workspace}/*"]

    condition {
      test     = "StringEquals"
      variable = "s3:x-amz-acl"
      values   = ["bucket-owner-full-control"]
    }

    principals {
      type        = "Service"
      identifiers = ["logs.ap-northeast-1.amazonaws.com"]
    }
  }
}
