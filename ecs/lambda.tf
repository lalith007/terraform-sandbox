# Lambda(Export Cloudwatch Logs)
data "archive_file" "export_cloudwatch_logs" {
    type        = "zip"
    source_dir  = "lambda/export_cloudwatch_logs"
    output_path = "lambda/export_cloudwatch_logs.zip"
}

resource "aws_lambda_function" "export_cloudwatch_logs" {
  filename         = "lambda/export_cloudwatch_logs.zip"
  function_name    = "${var.service_name}-${terraform.workspace}-export_cloudwatch_logs"
  role             = "${aws_iam_role.export_cloudwatch_logs.arn}"
  handler          = "export_cloudwatch_logs.lambda_handler"
  source_code_hash = "${data.archive_file.export_cloudwatch_logs.output_base64sha256}"
  runtime          = "python2.7"

  environment {
    variables = {
      LOG_GROUP_NAME = "${aws_cloudwatch_log_group.main.name}"
      DESTINATION    = "${aws_s3_bucket.main.id}"
    }
  }

  tags {
    Name = "${var.service_name}-export_cloudwatch_logs"
  }
}

resource "aws_lambda_permission" "export_cloudwatch_logs" {
  statement_id   = "${var.service_name}-${terraform.workspace}-export_cloudwatch_logs"
  action         = "lambda:InvokeFunction"
  function_name  = "${aws_lambda_function.export_cloudwatch_logs.function_name}"
  principal      = "events.amazonaws.com"
  source_arn     = "${aws_cloudwatch_event_rule.export_cloudwatch_logs.arn}"
}

# CloudWatch Event(Export CloudWatch Logs)
resource "aws_cloudwatch_event_rule" "export_cloudwatch_logs" {
  name = "${var.service_name}-${terraform.workspace}-export_cloudwatch_logs"

  schedule_expression = "cron(5 15 * * ? *)" # UTC
}

resource "aws_cloudwatch_event_target" "snapshot" {
  rule = "${aws_cloudwatch_event_rule.export_cloudwatch_logs.name}"
  arn  = "${aws_lambda_function.export_cloudwatch_logs.arn}"
}
